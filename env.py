import sys
#sys.path.append('generate_surrogate_images/')
from generate_surrogate_images.axon_simulator import axon_simulator
import numpy as np
import cv2
import scipy
import random
from agent import tracker
import random
import math
import scipy.interpolate
import generate_surrogate_images.some_functions as sf

"""
the simulation environment to train the agent

"""
class axons_env(axon_simulator):
    def __init__(self, json_file_path, visual=False, size_of_tracker=11, size_of_large=21, precision=1, SAMPLE_POINTS=100):
        #ProduceImageBrain.__init__(self, 'path')
        super().__init__(json_file_path)
        self.visual = visual
        self.precision = precision
        """
        for the subpixel precision
        """
        self.SAMPLE_POINTS = 10 ** (self.precision + 1)
        self.size_of_tracker = size_of_tracker
        self.size_of_tracker_interp = size_of_tracker * (10 ** precision) - 1
        self.diff = int((self.size_of_tracker_interp - 1) / 2)
        # some parameters
        self.size_of_large = size_of_large
        self.size_of_large_interp = size_of_large * (10 ** precision) - 1
        self.diff_large = int((self.size_of_large_interp - 1) / 2)
        # build the agent...
        self.agent = tracker(visual=self.visual, precision=self.precision)

    # this will reset the environment...
    def reset(self):
        self.agent_stop = False
        self.track_success = False
        self.trackback_trigger_counter = 0
        # generate the axon images from the generator...
        _, self.binary_reward_map, self.entire_image = self.generate_axons()
        # add extra noise
        self.entire_image += np.random.normal(0.0493, 0.0327, (self.width + 2 * self.border_size, self.width + 2 * self.border_size))
        self.entire_image[self.entire_image < 0] = 0
        self.entire_image[self.entire_image > 1] = 1
        # copy one as the visual images...
        self.entire_image_visual = self.entire_image.copy()
        self.entire_image = 1 - self.entire_image
        # do the linear mapping to the ...
        #self.entire_image = sf.linear_mapping(self.entire_image)
        self.interpolate_image, self.interpolate_reward_map = self.interpolate_to_whole_image()
        # produce an noisy image which contain the previous path of the agent...
        self.previous_image = np.zeros(self.interpolate_image.shape)
        self.previous_image = 1 - self.previous_image
        # extra information for the critic... (and add some noise...)
        self.extra_information = self.interpolate_reward_map.copy()
        self.extra_information = 1 - self.extra_information
        # start point
        self.axons_start_point_x = int(round(self.control_point_of_mother_branch[0][0] + self.border_size, self.precision)) * (10 ** self.precision)
        self.axons_start_point_y = int(round(self.control_point_of_mother_branch[0][1] + self.border_size, self.precision)) * (10 ** self.precision)
        # end point
        self.axons_end_point_x = int(round(self.control_point_of_mother_branch[-1][0] + self.border_size, self.precision)) * (10 ** self.precision)
        self.axons_end_point_y = int(round(self.control_point_of_mother_branch[-1][1] + self.border_size, self.precision)) * (10 ** self.precision)
        # we should also have a init vector direction
        self.prev_dir_vec = [self.control_point_of_mother_branch[1][0] - self.control_point_of_mother_branch[0][0], self.control_point_of_mother_branch[1][1] - self.control_point_of_mother_branch[0][1]]
        # set the position of the agent...
        self.agent.pos_x = self.axons_start_point_x
        self.agent.pos_y = self.axons_start_point_y
        # some state of the environment...
        state_temp_1 = self.interpolate_image[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_1 = cv2.resize(state_temp_1, (self.size_of_tracker, self.size_of_tracker))
        state_temp_1 = np.stack([state_temp_1, state_temp_1, state_temp_1, state_temp_1], axis=0)
        # state_temp_2 is the 21x21 pixel around the agent and downsample to the 11x11...
        state_temp_2 = self.interpolate_image[self.agent.pos_y-self.diff_large:self.agent.pos_y-self.diff_large+self.size_of_large_interp, self.agent.pos_x-self.diff_large:self.agent.pos_x-self.diff_large+self.size_of_large_interp]
        state_temp_2 = cv2.resize(state_temp_2, (self.size_of_tracker, self.size_of_tracker))
        state_temp_2 = np.stack([state_temp_2, state_temp_2, state_temp_2, state_temp_2], axis=0)
        # state_temp_3 ---- the previous path of the environment...
        state_temp_3 = self.previous_image[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_3 = cv2.resize(state_temp_3, (self.size_of_tracker, self.size_of_tracker))
        state_temp_3 = np.stack([state_temp_3, state_temp_3, state_temp_3, state_temp_3], axis=0)
        # create the third states
        state_temp_4 = self.extra_information[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_4 = cv2.resize(state_temp_4, (self.size_of_tracker, self.size_of_tracker))
        state_temp_4 = np.stack([state_temp_4, state_temp_4, state_temp_4, state_temp_4], axis=0)
        # concatenate them together....
        self.state = np.concatenate((state_temp_1, state_temp_2, state_temp_3), axis=0)
        self.state_extra = np.concatenate((self.state, state_temp_4), axis=0)
        # generate two states, one is for actor, one is for the critic
        return self.state, self.state_extra

    # step funciton
    def step(self, actions):
        # take down the previous position of the agent...
        prev_pos_x = self.agent.pos_x
        prev_pos_y = self.agent.pos_y
        # do some changes to the actions.... due to the subsample...
        actions_excuted = actions.copy()
        # because we are in the subpixel level!!!!
        actions_excuted[0] = round(actions_excuted[0], self.precision) * (10 ** self.precision)
        actions_excuted[1] = round(actions_excuted[1], self.precision) * (10 ** self.precision)
        # excute the actions...
        self.agent.move(actions_excuted)
        # get the reward from the ...
        reward, path_recorder = self.reward_function(prev_pos_x, prev_pos_y)
        # add the previous path...
        self.add_the_previous_path(path_recorder)
        # check the terminal...
        terminal = self.check_the_terminal()
        # prepare to the next state....
        state_temp_1 = self.state[0:3, :, :]
        state_temp_1_next = self.interpolate_image[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_1_next = cv2.resize(state_temp_1_next, (self.size_of_tracker, self.size_of_tracker))
        state_temp_1_next = state_temp_1_next[np.newaxis, :, :]
        state_temp_1 = np.concatenate((state_temp_1_next, state_temp_1), axis=0)
        # process the second states
        state_temp_2 = self.state[4:7, :, :]
        state_temp_2_next = self.interpolate_image[self.agent.pos_y-self.diff_large:self.agent.pos_y-self.diff_large+self.size_of_large_interp, self.agent.pos_x-self.diff_large:self.agent.pos_x-self.diff_large+self.size_of_large_interp]
        state_temp_2_next = cv2.resize(state_temp_2_next, (self.size_of_tracker, self.size_of_tracker))
        state_temp_2_next = state_temp_2_next[np.newaxis, :, :]
        state_temp_2 = np.concatenate((state_temp_2_next, state_temp_2), axis=0)
        # process the thrid states
        state_temp_3 = self.state[8:11, :, :]
        state_temp_3_next = self.previous_image[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_3_next = cv2.resize(state_temp_3_next, (self.size_of_tracker, self.size_of_tracker))
        state_temp_3_next = state_temp_3_next[np.newaxis, :, :]
        state_temp_3 = np.concatenate((state_temp_3_next, state_temp_3), axis=0)
        # process the final states
        state_temp_4 = self.state_extra[12:15, :, :]
        state_temp_4_next = self.extra_information[self.agent.pos_y - self.diff:self.agent.pos_y-self.diff + self.size_of_tracker_interp, self.agent.pos_x - self.diff: self.agent.pos_x - self.diff + self.size_of_tracker_interp]
        state_temp_4_next = cv2.resize(state_temp_4_next, (self.size_of_tracker, self.size_of_tracker))
        state_temp_4_next = state_temp_4_next[np.newaxis, :, :]
        state_temp_4 = np.concatenate((state_temp_4_next, state_temp_4), axis=0)
        # process the states
        self.state = np.concatenate((state_temp_1, state_temp_2, state_temp_3), axis=0)
        self.state_extra = np.concatenate((self.state, state_temp_4), axis=0)
        # has two more things....
        return self.state, reward, terminal, self.state_extra

    # the function below are used to help the main functions.....
    def interpolate_to_whole_image(self):
        x_axis = np.arange(0, self.width + 2 * self.border_size)
        y_axis = np.arange(0, self.height + 2 * self.border_size)
        # create the interpolation function
        interpolate_function = scipy.interpolate.interp2d(x_axis, y_axis, self.entire_image)
        interpolate_function_reward = scipy.interpolate.interp2d(x_axis, y_axis, self.binary_reward_map)
        # start to do the iinterpolation
        x_axis_interp = np.arange(0, self.width + 2 * self.border_size, 1 / (10 ** self.precision))
        y_axis_interp = np.arange(0, self.height + 2 * self.border_size, 1 / (10 ** self.precision))
        # which we will do the tracking on???
        interpolate_image = interpolate_function(x_axis_interp, y_axis_interp)
        interpolate_reward_map = interpolate_function_reward(x_axis_interp, y_axis_interp)
        return interpolate_image, interpolate_reward_map

    # get the reward from the environment...
    def reward_function(self, prev_pos_x, prev_pos_y):
        reward = 0
        # it was used to recorder the previous path of the agent...
        path_recorder = []
        current_dir_vec, length_of_path = self.calculate_the_direction_vector(prev_pos_x, prev_pos_y)
        length_of_path = int(round(length_of_path))
        # calculate the dot product of two direction vector...
        dot_product = current_dir_vec[0] * self.prev_dir_vec[0] + current_dir_vec[1] * self.prev_dir_vec[1]

        if length_of_path == 0:
            # should not be stopped!!!
            path_recorder.append([prev_pos_x, prev_pos_y])
            path_recorder = np.array(path_recorder)
            reward = -1

        elif length_of_path == 1:
            path_recorder.append([prev_pos_x, prev_pos_y])
            path_recorder.append([self.agent.pos_x, self.agent.pos_y])
            path_recorder = np.array(path_recorder)
            reward = self.interpolate_reward_map[prev_pos_y, prev_pos_x] + self.interpolate_reward_map[self.agent.pos_y, self.agent.pos_x]
            if dot_product <= 0:
                # if goes to the opposite direction, the counter should add 1...
                self.trackback_trigger_counter += 1

            if self.trackback_trigger_counter % 2 != 0:
                reward = -1 * reward

            self.prev_dir_vec = current_dir_vec

        elif length_of_path > 1:
            #...start to process the points...
            processed_point_x = prev_pos_x
            processed_point_y = prev_pos_y
            path_recorder.append([processed_point_x, processed_point_y])
            for _ in range(self.SAMPLE_POINTS):
                processed_point_x += current_dir_vec[0]
                processed_point_y += current_dir_vec[1]
                #...append...
                path_recorder.append([math.floor(processed_point_x), math.floor(processed_point_y)])

            path_recorder.append([self.agent.pos_x, self.agent.pos_y])
            path_recorder = np.array(path_recorder)
            # remove the repeated term...
            path_recorder = np.unique(path_recorder, axis=0)
            for idx in range(path_recorder.shape[0]):
                reward += self.interpolate_reward_map[path_recorder[idx, 1], path_recorder[idx, 0]]

            if dot_product <= 0:
                self.trackback_trigger_counter += 1

            if self.trackback_trigger_counter % 2 != 0:
                reward = -1 * reward
            self.prev_dir_vec = current_dir_vec
        return reward, path_recorder

    # check if reach the terminal states
    def check_the_terminal(self):
        # calculate the distance to the end
        distance_to_end = math.sqrt((self.agent.pos_y - self.axons_end_point_y)**2 + (self.agent.pos_x - self.axons_end_point_x)**2)

        if(self.agent.pos_x >= self.diff_large + 5 * (10 ** self.precision) and self.agent.pos_x <= self.border_size * (10 ** self.precision) * 2 + self.width * (10 ** self.precision) - self.diff_large - 5 * (10 ** self.precision) and self.agent.pos_y >= self.diff_large + 5 * (10 ** self.precision) and 
                            self.agent.pos_y <= self.border_size * (10 ** self.precision) * 2 + self.height * (10 ** self.precision) - self.diff_large - 5 * (10 ** self.precision) and distance_to_end > 10 * (10 ** self.precision)):
            terminal = False
        else:
            terminal = True

            if(self.agent.pos_x >= self.diff_large + 5 * (10 ** self.precision) and self.agent.pos_x <= self.border_size * (10 ** self.precision) * 2 + self.width * (10 ** self.precision) - self.diff_large - 5 * (10 ** self.precision) and self.agent.pos_y >= self.diff_large + 5 * (10 ** self.precision) and 
                self.agent.pos_y <= self.border_size * (10 ** self.precision) * 2 + self.height * (10 ** self.precision) - self.diff_large - 5 * (10 ** self.precision) and distance_to_end <= 10 * (10 ** self.precision)):
                
                self.track_success = True
        return terminal

    # calculate the direction vector...
    def calculate_the_direction_vector(self, prev_pos_x, prev_pos_y):
        length = math.sqrt((self.agent.pos_x - prev_pos_x)**2 + (self.agent.pos_y - prev_pos_y)**2)
        unit_dir_vec_x = (self.agent.pos_x - prev_pos_x) / self.SAMPLE_POINTS
        unit_dir_vec_y = (self.agent.pos_y - prev_pos_y) / self.SAMPLE_POINTS
        return [unit_dir_vec_x, unit_dir_vec_y], length

    # add the previous path to the image....
    def add_the_previous_path(self, path_recorder):
        diff = int((10 ** self.precision) / 2)
        width = int((10 ** self.precision) + 1)
        for idx in range(path_recorder.shape[0]):
            self.previous_image[path_recorder[idx, 1] - diff:path_recorder[idx, 1] - diff + width, path_recorder[idx, 0] - diff:path_recorder[idx, 0] - diff + width] = 0

if __name__ == '__main__':

    env = AxonEnv('parameters_64x64_images.json')
    state_temp_1, _ = env.reset()

    cv2.imshow('img', env.extra_information)
    cv2.waitKey(0)
