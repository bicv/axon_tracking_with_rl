import argparse

# define the function to get the arguments...
def achieve_arguments():
    # TODO: Maybe find a better way to add the cuda...
    parse = argparse.ArgumentParser()
    parse.add_argument('--gamma', type=float, default=0.99, help='the discount value of the RL')
    parse.add_argument('--policy_lr', type=float, default=0.0005, help='the learning rate of the actor network')
    parse.add_argument('--value_lr', type=float, default=0.0005, help='the learning rate of the critic network')
    parse.add_argument('--tau', type=float, default=0.95, help='the coefficient to calculate the GAE')
    parse.add_argument('--epsilon', type=float, default=0.2, help='the coefficient to clip the surrogate function')
    parse.add_argument('--policy_update_step', type=int, default=10, help='the update step of the policy network')
    parse.add_argument('--value_update_step', type=int, default=10, help='the update step of the critic network')
    parse.add_argument('--batch_size', type=int, default=32, help='the batch size of the ppo')
    parse.add_argument('--episode_length', type=int, default=200, help='the maximum length per episode...')
    parse.add_argument('--saved_path', type=str, default='saved_models/', help='the path that save models')
    parse.add_argument('--model-name', type=str, default='model.pt', help='model name')

    args = parse.parse_args()

    return args 


