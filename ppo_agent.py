import numpy as np 
import torch
from models import actor_network, critic_network
from torch.distributions.beta import Beta
import models
import os
from datetime import datetime
"""
pytorch version is 1.0.1

"""
class ppo_agent():
    def __init__(self, env, args):
        # define the parameters...
        self.env = env
        self.args = args 
        # check if cuda is avaiable...
        self.use_cuda = torch.cuda.is_available()
        print('The cuda is avaiable: {}'.format(self.use_cuda))
        # also print the learning rate...
        print('The policy_lr is: ' + str(self.args.policy_lr) + ', and the value_lr is: ' + str(self.args.value_lr))
        # define the network...
        self.actor_net = actor_network()
        self.critic_net = critic_network()
        # if use the cuda...put them into the cuda tensor...
        if self.use_cuda:
            self.actor_net.cuda()
            self.critic_net.cuda()
        # define the optimizer
        self.actor_optim = torch.optim.Adam(self.actor_net.parameters(), lr=self.args.policy_lr, weight_decay=0.0003)
        self.critic_optim = torch.optim.Adam(self.critic_net.parameters(), lr=self.args.value_lr, weight_decay=0.0003)
        if not os.path.exists(args.saved_path):
            os.mkdir(args.saved_path)
    # start to train the network....
    def train(self):
        num_of_episode = 0
        for _ in range(1000):
            # set up a memory
            replay_memory = []
            batch_reward = 0
            # every 100 eposide update once ...
            for ep in range(self.args.batch_size):
                state, state_extra = self.env.reset()
                reward_sum = 0
                # time steps in one game...
                for _ in range(self.args.episode_length):
                    state_tensor = torch.tensor(state, dtype=torch.float32).unsqueeze(0)
                    if self.use_cuda:
                        state_tensor = state_tensor.cuda()
                    # build up the beta distribution according to the alpha and beta....
                    action_alpha, action_beta = self.actor_net(state_tensor)
                    action_selected = self.action_selection(action_alpha, action_beta)
                    # easy for store!!!!!!!!!!
                    action_selected = action_selected.detach().cpu().numpy().squeeze()
                    # process the processed actions which between -4 and 4...
                    action_clipped = -4 + action_selected * 8
                    state_, reward, done, state_extra_ = self.env.step(action_clipped)
                    # sum of the reward
                    reward_sum += reward
                    # store the transitions...
                    replay_memory.append((state, state_extra, action_selected, reward, done))
                    if done:
                        break
                    state = state_
                    state_extra = state_extra_
                # accumlate the reward
                batch_reward += reward_sum
            # now we can do the update...
            critic_loss, actor_loss = self.update_network(replay_memory)
            batch_reward = batch_reward / self.args.batch_size
            print('[{}] the epoch is: {}, the reward mean is: {:.3f}, the critic_loss is: {:.3f}, the actor loss is: {:.3f}'.format(datetime.now(), num_of_episode, \
                        batch_reward, critic_loss, actor_loss))
            if num_of_episode % 10 == 0:
                # save the policy network...
                path_policy = self.args.saved_path + 'model_' + str(num_of_episode) + '.pt'
                torch.save(self.actor_net.state_dict(), path_policy)
            num_of_episode += 1

    # update the whole network...
    def update_network(self, memory):
        # process the state batch...
        state_batch = np.array([element[0] for element in memory])
        state_batch_actor_tensor = torch.tensor(state_batch, dtype=torch.float32)
        # for the extra information
        state_extra_batch = np.array([element[1] for element in memory])
        state_batch_critic_tensor = torch.tensor(state_extra_batch, dtype=torch.float32)
        # process the reward batch...
        reward_batch = np.array([element[3] for element in memory])
        reward_batch_tensor = torch.tensor(reward_batch, dtype=torch.float32)
        # process the done batch...
        done_batch = [element[4] for element in memory]
        # process the action batch
        action_batch = np.array([element[2] for element in memory])
        action_batch_tensor = torch.tensor(action_batch, dtype=torch.float32)
        # calculate the predicted value...
        if self.use_cuda:
            state_batch_actor_tensor = state_batch_actor_tensor.cuda()
            state_batch_critic_tensor = state_batch_critic_tensor.cuda()
            action_batch_tensor = action_batch_tensor.cuda()
        # calculate the predicted value....
        predicted_value = self.critic_net(state_batch_critic_tensor)
        predicted_value = predicted_value.detach().cpu()
        # calculate the estimated return value of the state...
        returns, advantages = self.calculate_the_discounted_reward(reward_batch_tensor, done_batch, predicted_value)
        # update the value network!!!
        loss_critic = self.update_critic_network(returns, state_batch_critic_tensor)
        loss_actor = self.update_actor_network(state_batch_actor_tensor, advantages, action_batch_tensor)
        return loss_critic.item(), loss_actor.item()
    # selection the actions according to the gaussian distribution...
    def action_selection(self, alpha, beta):
        actions = Beta(alpha.detach().cpu(), beta.detach().cpu()).sample()
        return actions
    # calculate the discounted reward...
    # add the GAE here....
    def calculate_the_discounted_reward(self, reward_batch_tensor, done_batch, predicted_value):
        returns = torch.Tensor(len(done_batch), 1)
        advantages = torch.Tensor(len(done_batch), 1)
        deltas = torch.Tensor(len(done_batch), 1)
        # get the previous returns 
        previous_return = 0
        previous_value = 0
        previous_advantages = 0
        for index in reversed(range(len(done_batch))):
            if done_batch[index]:
                returns[index, 0] = reward_batch_tensor[index]
                deltas[index, 0] = reward_batch_tensor[index] - predicted_value.data[index, 0]
                advantages[index, 0] = deltas[index, 0]
            else:
                returns[index, 0] = reward_batch_tensor[index] + self.args.gamma * previous_return
                deltas[index, 0] = reward_batch_tensor[index] + self.args.gamma * previous_value - predicted_value.data[index, 0]
                advantages[index, 0] = deltas[index, 0] + self.args.gamma * self.args.tau * previous_advantages
            # start to assign the previous value
            previous_return = returns[index, 0]
            previous_value = predicted_value.data[index, 0]
            previous_advantages = advantages[index, 0]
        # normalize the advantages...
        advantages = (advantages - advantages.mean()) / advantages.std()
        return returns, advantages
    # update the critic network....
    def update_critic_network(self, returns, state_batch_tensor):
        if self.use_cuda:
            targets = returns.cuda()
        for _ in range(self.args.value_update_step):
            predicted_value = self.critic_net(state_batch_tensor)
            loss = (predicted_value - targets).pow(2).mean()
            self.critic_optim.zero_grad()
            loss.backward()
            # update
            self.critic_optim.step()
        return loss
    # update the actor network....
    def update_actor_network(self, state_batch_tensor, advantages, action_batch_tensor):
        if self.use_cuda:
            advantages = advantages.cuda()
        # calculate the old probabilities...
        action_alpha_old, action_beta_old = self.actor_net(state_batch_tensor)
        beta_dist_old = Beta(action_alpha_old, action_beta_old)
        old_action_prob = beta_dist_old.log_prob(action_batch_tensor).sum(1, keepdim=True)
        # detach it from the computation graph...
        old_action_prob = old_action_prob.detach()
        for _ in range(self.args.policy_update_step):
            action_alpha, action_beta = self.actor_net(state_batch_tensor)
            beta_dist_new = Beta(action_alpha, action_beta)
            new_action_prob = beta_dist_new.log_prob(action_batch_tensor).sum(1, keepdim=True)    
            # calculate the ratio
            ratio = torch.exp(new_action_prob - old_action_prob)
            # calculate the surr
            surr1 = ratio * advantages
            surr2 = torch.clamp(ratio, 1 - self.args.epsilon, 1 + self.args.epsilon) * advantages
            loss = -torch.min(surr1, surr2).mean()
            self.actor_optim.zero_grad()
            loss.backward()
            # update
            self.actor_optim.step()
        return loss
