# This is the new version of our dataset simulator...
# If you have any quesitons, just contact me...
import numpy as np 
import random
import generate_surrogate_images.utils as utils

# the first place which has been revisied...
def pixel_distance_to_axon(width, height, axon_gt_points_without_gap, thickness, min_gap_size, max_gap_size, variations, variations_gt):
	num_of_central_points = axon_gt_points_without_gap.shape[1]
	middle_points = random.randint(0, num_of_central_points - 1)
	gap_size = random.randint(min_gap_size, max_gap_size)

	case = False
	branch_dis_without_gap = float('inf') * np.ones((height, width))
	branch_variations_without_gap = float('inf') * np.ones((height, width))
	branch_variations_with_gap = float('inf') * np.ones((height, width))

	# use the meshgrid... which use the function in the utils before, but this is better i think...
	capital_x, capital_y = np.meshgrid(np.arange(width), np.arange(height))

	if gap_size > 0:
		case = True
		if middle_points > gap_size and middle_points < num_of_central_points - gap_size:
			# the gap is in the middle of the image...
			indices_gap_points = np.arange(middle_points - gap_size, middle_points + gap_size)

		elif middle_points <= gap_size:
			# start by the gaps... (But I am not sure if it is worked with our tracking problems...)
			indices_gap_points = np.arange(0, middle_points + gap_size)
		
		elif middle_points >= num_of_central_points - gap_size:
			#the axons ended by a gap....
			indices_gap_points = np.arange(middle_points - gap_size, width)

		new_axon_gt_points = axon_gt_points_without_gap.copy()
		new_variations = variations.copy()

		# gaps?
		new_axon_gt_points[:, indices_gap_points] = float('inf')
		new_variations[indices_gap_points] = float('inf')

	else:
		new_axon_gt_points = axon_gt_points_without_gap.copy()
		new_variations = variations.copy()
		new_variations_gt = variations_gt.copy()


	for index in range(num_of_central_points):
		distance_matrix = utils.distance_calcu(capital_x, capital_y, new_axon_gt_points[:, index])
		index_satisfy = np.where(distance_matrix < thickness)
		if index_satisfy[0].size != 0:
			for num_of_index in range(index_satisfy[0].size):
				this_row = index_satisfy[0][num_of_index]
				this_col = index_satisfy[1][num_of_index]
				a = min(branch_dis_without_gap[this_row, this_col], distance_matrix[this_row, this_col])

				if a == distance_matrix[this_row, this_col]:
					branch_dis_without_gap[this_row, this_col] = a
					branch_variations_with_gap[this_row, this_col] = new_variations[index]
					branch_variations_without_gap[this_row, this_col] = new_variations_gt[index]

	branch_dis_with_gap = branch_dis_without_gap.copy()
	#branch_variations_with_gap = branch_variations_without_gap.copy()

	if case:
		for index in range(num_of_central_points):
			distance_matrix = utils.distance_calcu(capital_x, capital_y, axon_gt_points_without_gap[:, index])
			index_satisfy = np.where(distance_matrix < thickness)
			if index_satisfy[0].size != 0:
				for num_of_index in range(index_satisfy[0].size):
					this_row = index_satisfy[0][num_of_index]
					this_col = index_satisfy[1][num_of_index]
					a = min(branch_dis_without_gap[this_row, this_col], distance_matrix[this_row, this_col])

					if a == distance_matrix[this_row, this_col]:
						branch_dis_without_gap[this_row, this_col] = a
						branch_variations_without_gap[this_row, this_col] = variations[index]

	return branch_dis_without_gap, branch_dis_with_gap, branch_variations_with_gap, branch_variations_without_gap, gap_size, middle_points

# used for linear mapping...
def linear_mapping(images):
	max_value = images.max()
	min_value = images.min()

	parameter_a = 1 / (max_value - min_value)
	parameter_b = 1 - max_value * parameter_a

	image_after_mapping = parameter_a * images + parameter_b

	return image_after_mapping

def linear_mapping_special(images, max_value, min_value):

	parameter_a = 1 / (max_value - min_value)
	parameter_b = 1 - max_value * parameter_a

	image_after_mapping = parameter_a * images + parameter_b

	return image_after_mapping










































