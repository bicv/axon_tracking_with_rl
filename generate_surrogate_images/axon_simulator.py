from __future__ import division
import numpy as np
from scipy import interpolate
import json
import random
import generate_surrogate_images.utils as utils
import math
import cmath
import cv2
import scipy
from scipy.ndimage import gaussian_filter
import generate_surrogate_images.some_functions as sf
np.set_printoptions(threshold = 1e6)

'''
This code is re-implement version of the project by Billot: https://github.com/BBillot/generative_model_and_unet/

'''
class axon_simulator():
    def __init__(self, paths):

        # load the parameters from the json files
        self.paths = paths
        self._get_params()

    # load the json files...
    def load_the_json_files(self, path):
        with open(path, 'r') as f:
            parameters = json.loads(f.read())
        return parameters

    # the mother branch of the axons...
    def generate_axons(self):
        restart = False
        while restart == False:
            num_of_axons = random.randint(self.min_axons, self.max_axons)  # number of axons
            num_of_branches = np.random.randint(self.min_bran, self.max_bran + 1, size=(1, num_of_axons))  # number of branches per axons
            thickness = np.random.randint(round(self.min_thickness*100), round(self.max_thickness*100) + 1,
                                        size=(1, np.sum(num_of_branches))) / 100  # thickness of different branches...
            gap_size = np.zeros((1, np.sum(num_of_branches)))  # size of gaps in each branches...
            total_points = np.sum(num_of_branches) * self.NSpline_points  # number of interpolating points
            axons_GT_points = np.zeros((5, total_points))  # vector of the interpolating points
            variations = np.zeros((1, total_points))  # vector of the intensity variations
            variations_gt = np.zeros((1, total_points))
            pointer = 0  # points to the current branch
            pointer_axons = 0  # points to the start of the current axons in the GT points

            axons_dis_without_gap = float('inf') * np.ones((self.height, self.width))
            axons_dis_with_gap = float('inf') * np.ones((self.height, self.width))

            axons_variations_without_gap = float('inf') * np.ones((self.height, self.width))
            axons_variations = float('inf') * np.ones((self.height, self.width))

            for axon_index in range(num_of_axons):   # should be num_of_axons
                if axon_index > 0:
                    pointer_axons = pointer_axons + num_of_branches[0, axon_index - 1] * self.NSpline_points
                ncross = 0
                cross = True
                while cross:
                    if self.crossing_ok:  # if axons can cross, we just get the start point...
                        start_x, start_y, v = self.get_start_coords(self.width, self.height)
                        cross = False
                    else:  # if they can't, we need to check if the randomly picked points is not already on the existing branch
                        start_ok = False
                        while start_ok == False:

                            start_x, start_y, v = self.get_start_coords(self.width, self.height)
                            if axons_dis_without_gap[start_x, start_y] == float('inf'):
                                start_ok = True
                    if axon_index == 0:
                        # record the start point...
                        start_x_record = start_x
                        start_y_record = start_y

                    xp = start_x
                    yp = start_y

                    control_point = [[xp, yp]]
                    at_terminate_state = False

                    while at_terminate_state == False:
                        v = self.get_valid_direction(v, self.conformity)
                        #print v.shape
                        xp += self.step_size * v[0, 0]
                        yp += self.step_size * v[0, 1]


                        if (xp <= 0 or xp >= self.width - 1 or yp <= 0 or yp >= self.height - 1):
                            at_terminate_state = True

                        xp = min(xp, self.width - 1)
                        xp = max(xp, 0)

                        yp = min(yp, self.height - 1)
                        yp = max(yp, 0)

                        control_point.append([xp, yp])  # store the control point...

                    # we can get the control points here!!!!
                    if axon_index == 0:
                        num_of_points = len(control_point)
                        x_end = control_point[num_of_points - 1][0]
                        y_end = control_point[num_of_points - 1][1]

                    self.control_point_of_mother_branch = control_point


                    interpolation_points = self.get_axon_GT_points(np.array(control_point), self.NSpline_points)

                    # basline point
                    self.baseline_point = interpolation_points.copy()

                    axons_GT_points[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = interpolation_points[:, 0]
                    axons_GT_points[1, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = interpolation_points[:, 1]
                    axons_GT_points[2, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = axon_index
                    axons_GT_points[3, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = 0
                    axons_GT_points[4, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = pointer


                    # fill the variation vector for this branch...
                    variations[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points], variations_gt[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = self.make_variation(
                        random.randint(self.min_axon_intensity, self.max_axon_intensity) / 100, self.axon_profile, self.NSpline_points,
                        self.min_axon_intensity, self.max_axon_intensity, self.min_period, self.max_period
                    )

                    branch_dis_without_gap, branch_dis_with_gap, branch_variations, branch_variations_without_gaps, gap_size[0, pointer], gap_index = sf.pixel_distance_to_axon(
                        self.width, self.height, axons_GT_points[0:2, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points],
                        thickness[0, pointer], self.min_gap_size, self.max_gap_size,
                        variations[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points],
                        variations_gt[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points]
                    )

                    # get the gap middle point...
                    self.gap_pos_x = interpolation_points[:, 0][gap_index]
                    self.gap_pos_y = interpolation_points[:, 1][gap_index]
                    
                    ###################################################################

                    the_size_of_inf_check = np.where((branch_dis_without_gap != float('inf')) & (axons_dis_without_gap != float('inf')) == True)[0]

                    if self.crossing_ok == False and the_size_of_inf_check.size == 0:
                        cross = False

                    else:
                        ncross += 1

                    if ncross > 50:
                        restart = True

                    if restart == True:
                        break
                if restart == True:
                    break
                # update the axons distance with gap...
                axons_dis_with_gap = utils.get_the_min_value_between_two_matrix(axons_dis_with_gap, branch_dis_with_gap)
                axons_dis_without_gap = utils.get_the_min_value_between_two_matrix(axons_dis_without_gap, branch_dis_without_gap)
                axons_variations = utils.get_the_min_value_between_two_matrix(axons_variations, branch_variations)
                axons_variations_without_gap = utils.get_the_min_value_between_two_matrix(axons_variations_without_gap,
                                                                                          branch_variations_without_gaps)

                pointer += 1  # this means the pointer to the branch...
                for branch_index in range(1, num_of_branches[0, axon_index]):
                    cross = True
                    while cross:
                        random_start_point = random.randint(pointer_axons, pointer_axons + self.NSpline_points * branch_index)
                        new_start = axons_GT_points[0:2, random_start_point]
                        xp = new_start[0]
                        yp = new_start[1]
                        control_point = [[xp, yp]]
                        at_terminate_state = False
                        while at_terminate_state == False:
                            v = self.get_valid_direction(v, self.conformity)
                            xp += self.step_size * v[0, 0]
                            yp += self.step_size * v[0, 1]

                            if (xp <= 0 or xp >= self.width - 1 or yp <= 0 or yp >= self.height - 1):
                                at_terminate_state = True
                            xp = min(xp, self.width - 1)
                            xp = max(xp, 0)
                            yp = min(yp, self.height - 1)
                            yp = max(yp, 0)
                            control_point.append([xp, yp])  # store the control point...
                        interpolation_points = self.get_axon_GT_points(np.array(control_point), self.NSpline_points)
                        axons_GT_points[0, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = interpolation_points[:, 0]
                        axons_GT_points[1, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = interpolation_points[:, 1]
                        axons_GT_points[2, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = axon_index
                        axons_GT_points[3, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = branch_index
                        axons_GT_points[4, pointer * self.NSpline_points:pointer*self.NSpline_points + self.NSpline_points] = pointer
                        # calcuilate the variations
                        variations[0, pointer*self.NSpline_points:pointer*self.NSpline_points+self.NSpline_points] = self.make_variation(
                            variations[0, random_start_point], self.branch_profile, self.NSpline_points, self.min_axon_intensity,
                            self.max_axon_intensity, self.min_period, self.max_period
                        )
                        # get the pxiel intensity distance
                        branch_dis_without_gap, branch_dis_with_gap, branch_variations, branch_variations_without_gaps, gap_size[0, pointer], gap_index = sf.pixel_distance_to_axon(
                            self.width, self.height, axons_GT_points[0:2, pointer * self.NSpline_points:pointer * self.NSpline_points + self.NSpline_points],
                            thickness[0, pointer], self.min_gap_size, self.max_gap_size,
                            variations[0, pointer * self.NSpline_points:pointer * self.NSpline_points + self.NSpline_points]
                        )
                        if self.crossing_ok:
                            cross = False
                        else:
                            cross = self.check_crossing(axons_dis_without_gap, branch_dis_without_gap,
                                                        self.straight_branching*thickness[0, pointer], new_start)
                            if cross:
                                ncross += 1

                        if ncross > 50:
                            restart = True
                        if restart:
                            break
                    if restart:
                        break
                    axons_dis_with_gap = utils.get_the_min_value_between_two_matrix(axons_dis_with_gap, branch_dis_with_gap)
                    axons_dis_without_gap = utils.get_the_min_value_between_two_matrix(axons_dis_without_gap, branch_dis_without_gap)
                    axons_variations = utils.get_the_min_value_between_two_matrix(axons_variations, branch_variations)
                    axons_variations_without_gap = utils.get_the_min_value_between_two_matrix(axons_variations_without_gap, branch_variations_without_gaps)
                    pointer += 1
                if restart:
                    break
            if restart:
                restart = False
                continue
            else:
                restart = True
        # process the distance matrix
        axons_patch, axons_patch_without_gap = self.process_the_obtained_matrix(axons_dis_with_gap, axons_dis_without_gap,
                                                                                axons_variations, axons_variations_without_gap)
        if self.add_bouton_requirement:
            axons_patch, axons_patch_without_bouton, bouton_patch = self.add_bouton(axons_patch, axons_GT_points, self.min_NbBouton, self.max_NbBouton, self.min_bou_radius,
                                   self.max_bou_radius, self.min_brightness_bouton, self.max_brightness_bouton, self.sigma_noise_bouton,
                                   self.height, self.width, thickness)

        if self.add_cell_requirement:
            axons_patch = self.add_cell(axons_patch, self.height, self.width, self.sigma_noise_circle, self.min_NbCircles,
                                        self.max_NbCircles, self.circle_brightness, self.min_brightness_circles, self.max_brightness_circles,
                                        self.min_radius, self.max_radius)

        # might be written as a function in the future?
        if self.add_border:
            # for the real_image...
            border_image_width = self.border_size * 2 + self.width
            border_image_height = self.border_size * 2 + self.height
            border_image = np.zeros((border_image_height, border_image_width))
            border_image[self.border_size:self.border_size+self.height, self.border_size:self.border_size+self.width] = axons_patch
            axons_patch = border_image

            # for the image without_bouton
            border_image_without_bouton = np.zeros((border_image_height, border_image_width))
            border_image_without_bouton[self.border_size:self.border_size+self.height, self.border_size:self.border_size+self.width] = axons_patch_without_bouton
            axons_patch_without_bouton = border_image_without_bouton

            # for the image of ground_truth map
            border_image_ground_truth = np.zeros((border_image_height, border_image_width))
            border_image_ground_truth[self.border_size:self.border_size+self.height, self.border_size:self.border_size+self.width] = axons_patch_without_gap
            axons_patch_without_gap = border_image_ground_truth


        if self.add_noise_requirement:
            axons_patch, axons_patch_without_bouton = self.add_different_noise(axons_patch, axons_patch_without_bouton, self.sigma_noise_min, self.sigma_noise_max, self.lambda_min,
                                                   self.lambda_max, self.height, self.width)

        # convert the image into the gray_scale...
        # we need a different way to transerfer it into gray scale...
        axons_patch_without_bouton = np.floor(axons_patch_without_bouton * 255 / np.max(axons_patch))
        axons_patch = self.convert_the_image_into_gray_scale(axons_patch)

        #axons_patch_without_gap[axons_patch_without_gap <= 0] = 0
        axons_patch_without_gap = self.convert_the_image_into_gray_scale(axons_patch_without_gap)
        axons_patch_without_gap /= 255

        # we need also do some post processing:....
        max_value = axons_patch.max()
        min_value = axons_patch.min()

        # linear mapping
        axons_patch = sf.linear_mapping(axons_patch)
        axons_patch_without_bouton = sf.linear_mapping_special(axons_patch_without_bouton, max_value, min_value)
        # if transfer image into negative images
        if self.negative_image:
            axons_patch = self.convert_to_the_negative_image(axons_patch)

        return axons_patch, axons_patch_without_gap, axons_patch_without_bouton

    def process_the_obtained_matrix(self, axons_dis_with_gap, axons_dis_without_gap, axons_variations,
                                    axons_variations_without_gap):

        axons_variations[axons_variations == float('inf')] = 0
        axons_variations_without_gap[axons_variations_without_gap == float('inf')] = 0

        # add noise to the axons_variations...
        axons_variations += self.sigma_noise_axon * np.random.randn(self.width, self.height)
        axons_variations_without_gap += self.sigma_noise_axon * np.random.randn(self.width, self.height)

        axons_patch = self.varing_intensity_with_distance(axons_dis_with_gap, 'axons', 'gauss',
                                                          self.sigma_spread, axons_variations)

        axons_patch_without_gap = self.varing_intensity_with_distance(axons_dis_without_gap, 'axons', 'gauss',
                                                                      self.sigma_spread, axons_variations_without_gap)

        return axons_patch, axons_patch_without_gap

    def add_different_noise(self, axons_patch, axons_patch_without_bouton, sigma_noise_min, sigma_noise_max, lambda_min, lambda_max, height, width):
        sigma_noise = random.randint(round(sigma_noise_min * 100000), round(sigma_noise_max * 100000)) / 100000
        lambda_temp = random.randint(lambda_min, lambda_max)

        result, axons_patch_without_bouton = self.noise(axons_patch, axons_patch_without_bouton, sigma_noise, lambda_temp, height, width)

        return result, axons_patch_without_bouton

    # has been checked!!!!!!! (has been modified....)
    def add_bouton(self, axons_patch, axons_gt_points, min_num_bouton, max_num_bouton, min_bou_r, max_bou_r, min_bou_brightness,
                   max_bou_brightness, bou_sigma, height, width, thickness):

        num_of_bou = random.randint(min_num_bouton, max_num_bouton)
        axons_patch_without_bouton = axons_patch.copy()

        for index in range(num_of_bou):
            bou_brightness = random.randint(min_bou_brightness, max_bou_brightness) / 100
            point = random.randint(0, axons_gt_points.shape[1] - 1)
            center = np.round(np.array([axons_gt_points[0, point], axons_gt_points[1, point]]))

            #radius = math.floor(thickness[0, int(axons_gt_points[4, point])]) + 1
            radius = random.randint(min_bou_r, max_bou_r)
            dev = 0

            center[center + dev < 0] = 0
            center[center > width] = width

            # middle ...
            if center[0]-radius >= 0 and center[1]-radius >= 0 and center[0]+radius <= width - 1 and center[1]+radius <= height-1:
                abs_inf = center[0] - radius
                abs_sup = center[0] + radius

                ord_inf = center[1] - radius
                ord_sup = center[1] + radius

                ref_abs = radius
                ref_ord = radius

            # top left
            elif center[0] - radius < 0 and center[1] - radius < 0:
                abs_inf = 0
                abs_sup = center[0] + radius

                ord_inf = 0
                ord_sup = center[1] + radius

                ref_abs = center[0]
                ref_ord = center[1]

            # bottom left
            elif center[1] + radius > width - 1 and center[0] - radius < 0:
                abs_inf = 0
                abs_sup = center[0] + radius

                ord_inf = center[1] - radius
                ord_sup = height - 1

                ref_abs = center[0]
                ref_ord = radius

            # top right
            elif center[1] - radius < 0 and center[0] + radius > height - 1:
                abs_inf = center[0] - radius
                abs_sup = width - 1

                ord_inf = 0
                ord_sup = center[1] + radius

                ref_abs = radius
                ref_ord = center[1]

            # bottom right
            elif center[0] + radius > width - 1 and center[1] + radius > height - 1:
                abs_inf = center[0] - radius
                abs_sup = width - 1

                ord_inf = center[1] - radius
                ord_sup = height - 1

                ref_abs = radius
                ref_ord = radius

            # top
            elif center[1] - radius < 0:
                abs_inf = center[0] - radius
                abs_sup = center[0] + radius

                ord_inf = 0
                ord_sup = center[1] + radius

                ref_abs = radius
                ref_ord = center[1]

            # bottom
            elif center[1] + radius > width - 1:
                abs_inf = center[0] - radius
                abs_sup = center[0] + radius

                ord_inf = center[1] - radius
                ord_sup = height - 1

                ref_abs = radius
                ref_ord = radius

            # left
            elif center[0] - radius < 0:
                abs_inf = 0
                abs_sup = center[0] + radius

                ord_inf = center[1] - radius
                ord_sup = center[1] + radius

                ref_abs = center[0]
                ref_ord = radius

            # right
            elif center[0] + radius > width - 1:
                abs_inf = center[0] - radius
                abs_sup = width - 1

                ord_inf = center[1] - radius
                ord_sup = center[1] + radius

                ref_abs = radius
                ref_ord = radius


            input_height = int(ord_sup - ord_inf + 1)
            input_width = int(abs_sup - abs_inf + 1)
            x_capital, y_capital = utils.mesh_grid(input_width, input_height)

            bouton_dist = np.sqrt((x_capital - ref_abs)**2 + (y_capital - ref_ord)**2)
            v = math.sqrt(-(radius)**2 / (2 * math.log(0.05 / bou_brightness)))
            indices = np.where(bouton_dist > radius)

            for i in range(indices[0].size):
                this_row = indices[0][i]
                this_col = indices[1][i]
                bouton_dist[this_row, this_col] = float('inf')

            #bouton_dist = bouton_dist + bou_sigma * np.random.randn(int(ord_sup - ord_inf + 1), int(abs_sup - abs_inf + 1))
            bouton_dist = self.varing_intensity_with_distance(bouton_dist, 'circle', 'gauss', v, 0, bou_brightness)

            # maybe this needs to be optimized ...
            for row_index in range(int(ord_inf), int(ord_sup + 1)):
                for col_index in range(int(abs_inf), int(abs_sup + 1)):
                    axons_patch[row_index, col_index] = max(axons_patch[row_index, col_index], bouton_dist[row_index-int(ord_inf), col_index-int(abs_inf)])
                    
        return axons_patch, axons_patch_without_bouton, bouton_dist

    # add the cell .... (this part has been checked!!!!)
    def add_cell(self, axons_patch, height, width, sigma_noise_circle, min_num_circles,
                 max_num_circles, circle_brightness, min_brightness_circle, max_brightness_circle, min_radius, max_radius):

        new_axons_patch = axons_patch.copy()
        num_of_circle = random.randint(min_num_circles, max_num_circles)

        for circle_index in range(num_of_circle):

            cross = True
            radius = random.randint(min_radius, max_radius)
            selection_pixel = random.randint(1, 1 + circle_brightness)
            brightness = random.randint(min_brightness_circle, max_brightness_circle) / 100
            n_cross = 0
            jump_out = False

            while cross:
                center = np.zeros((1, 2)).astype(int)
                center[0, 0] = random.randint(0, width - 1)
                center[0, 1] = random.randint(0, height - 1)

                if center[0, 0] - radius >= 0 and center[0, 1] - radius >= 0 and center[0, 0] + radius < width and center[0, 1] + radius < height: # in the middle
                    if (axons_patch[center[0, 1]-radius:center[0, 1]+radius+1, center[0, 0]-radius:center[0, 0]+radius+1] == np.zeros((2*radius+1, 2*radius+1))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = center[0, 0] + radius

                        ord_inf = center[0, 1] - radius
                        ord_sup = center[0, 1] + radius

                        ref_abs = radius
                        ref_ord = radius

                elif center[0, 0] - radius < 0 and center[0, 1] - radius < 0:  # top left ...
                    if (axons_patch[0:center[0, 1]+radius, 0:center[0, 0]+radius] == np.zeros((center[0, 1]+radius, center[0, 0]+radius))).all():
                        cross = False
                        abs_inf = 0
                        abs_sup = center[0, 0] + radius

                        ord_inf = 0
                        ord_sup = center[0, 1] + radius

                        ref_abs = center[0, 0]
                        ref_ord = center[0, 1]

                elif center[0, 1] + radius >= height and center[0, 0] - radius < 0:  # bottom left...
                    if (axons_patch[center[0, 1]-radius:height, 0:center[0, 0]+radius] == np.zeros((height+radius-center[0, 1], center[0, 0]+radius))).all():
                        cross = False
                        abs_inf = 0
                        abs_sup = center[0, 0] + radius

                        ord_inf = center[0, 1] - radius
                        ord_sup = height - 1

                        ref_abs = center[0, 0]
                        ref_ord = radius

                elif center[0, 1] - radius < 0 and center[0, 0] + radius >= width:  # top right
                    if (axons_patch[0:center[0, 1]+radius, center[0,0]-radius:width] == np.zeros((center[0, 1]+radius, radius+width-center[0, 0]))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = width - 1

                        ord_inf = 0
                        ord_sup = center[0, 1] + radius

                        ref_abs = radius
                        ref_ord = center[0, 1]

                elif center[0, 0] + radius >= width and center[0, 1] + radius >= height:  # bottom right
                    if (axons_patch[center[0, 1]-radius:height, center[0, 0]-radius:width] == np.zeros((radius+height-center[0,1], radius+width-center[0, 0]))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = width - 1

                        ord_inf = center[0, 1] - radius
                        ord_sup = height - 1

                        ref_abs = radius
                        ref_ord = radius

                elif center[0, 1] - radius < 0:  # top
                    if (axons_patch[0:center[0, 1]+radius, center[0, 0]-radius:center[0,0]+radius+1] == np.zeros((center[0, 1]+radius, 2*radius+1))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = center[0, 0] + radius

                        ord_inf = 0
                        ord_sup = center[0, 1] + radius

                        ref_abs = radius
                        ref_ord = center[0, 1]

                elif center[0, 1] + radius >= height:  # bottom
                    if (axons_patch[center[0, 1]-radius:height, center[0, 0]-radius:center[0, 0]+radius+1] == np.zeros((radius+height-center[0, 1], 2*radius+1))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = center[0, 0] + radius

                        ord_inf = center[0, 1] - radius
                        ord_sup = height - 1

                        ref_abs = radius
                        ref_ord = radius

                elif center[0, 0] - radius < 0:  # left
                    if (axons_patch[center[0, 1]-radius:center[0, 1]+radius+1, 0:center[0, 0]+radius] == np.zeros((2*radius+1, center[0, 0]+radius))).all():
                        cross = False
                        abs_inf = 0
                        abs_sup = center[0, 0] + radius

                        ord_inf = center[0, 1] - radius
                        ord_sup = center[0, 1] + radius

                        ref_abs = center[0, 0]
                        ref_ord = radius

                elif center[0, 0] + radius >= width:  # right
                    if (axons_patch[center[0, 1]-radius:center[0, 1]+radius+1, center[0, 0]-radius:width] == np.zeros((2*radius+1, radius+width-center[0, 0]))).all():
                        cross = False
                        abs_inf = center[0, 0] - radius
                        abs_sup = width - 1

                        ord_inf = center[0, 1] - radius
                        ord_sup = center[0, 1] + radius

                        ref_abs = radius
                        ref_ord = radius

                n_cross += 1
                if n_cross >= 50: # add the limitations ...
                    jump_out = True
                    break

            if jump_out:
                break


            input_height = int(ord_sup - ord_inf + 1)
            input_width = int(abs_sup - abs_inf + 1)

            x_capital, y_capital = utils.mesh_grid(input_width, input_height)

            selection_matrix = np.random.randint(1, 1 + circle_brightness, size=(input_height, input_width))
            distance = np.sqrt((x_capital - ref_abs)**2 + (y_capital - ref_ord)**2)

            v = math.sqrt(-(radius)**2 / (2 * math.log(0.05 / brightness)))

            indices_selection = np.where(selection_matrix == selection_pixel)
            indices_radius = np.where(distance > radius)

            for index_selection in range(indices_selection[0].size):
                this_row = indices_selection[0][index_selection]
                this_col = indices_selection[1][index_selection]
                distance[this_row, this_col] = radius

            for index_radius in range(indices_radius[0].size):
                this_row = indices_radius[0][index_radius]
                this_col = indices_radius[1][index_radius]
                distance[this_row, this_col] = float('inf')

            distance += sigma_noise_circle * np.random.randn(input_height, input_width)
            distance = self.varing_intensity_with_distance(distance, 'circle', 'gauss', v, 1, brightness)


            for row_index in range(int(ord_inf), int(ord_sup+1)):
                for col_index in range(int(abs_inf), int(abs_sup+1)):
                    new_axons_patch[row_index, col_index] = max(new_axons_patch[row_index, col_index],
                                                                distance[row_index-int(ord_inf), col_index-int(abs_inf)])

        return new_axons_patch

    '''
    Above is the core function of then entire programme....
    Next is the assistant functions....
    '''
    # Randomly generates a starting on one of the four edges
    def get_start_coords(self, width, height):
        which_side = random.randint(1, 4)  # select which side to start...

        if which_side == 1:
            start_x = 0
            start_y = random.randint(round(height / 4), round(3 * height / 4))
            u0 = np.array([[1, 0]])

        elif which_side == 2:
            start_x = random.randint(round(width / 4), round(3 * width / 4))
            start_y = 0
            u0 = np.array([[0, 1]])

        elif which_side == 3:
            start_x = width - 1
            start_y = random.randint(round(height / 4), round(3 * height / 4))
            u0 = np.array([[-1, 0]])

        elif which_side == 4:
            start_x = random.randint(round(width / 4), round(3 * width / 4))
            start_y = height - 1
            u0 = np.array([[0, -1]])

        return start_x, start_y, u0

    def get_valid_direction(self, v, conformity):

        v = v / np.sqrt(np.sum(v ** 2))  # normlize the vector v ...
        no_valid_direction = True

        while no_valid_direction:
            u = self.make_rand_unit_dirvec(1)
            dp = np.dot(v, np.transpose(u))

            if dp > conformity:
                no_valid_direction = False
        return u

    def make_rand_unit_dirvec(self, N):
        dir_vec_temp = np.random.randn(N, 2)
        #  normalize the vector
        dir_vec = dir_vec_temp / np.sqrt(np.sum(dir_vec_temp ** 2))
        return dir_vec

    def get_axon_GT_points(self, control_points, num_of_points):
        temp = np.linspace(0, 1, control_points.shape[0])
        interpolate_function = interpolate.CubicSpline(temp, control_points)
        the_result_point = interpolate_function(np.linspace(0, 1, num_of_points))
        
        return the_result_point

    # need to be added more informations...(has some bugs...)
    def make_variation(self, start_variation, profile_type, number_of_spline_pointer, min_axon_intensity, max_axon_intensity,
                       min_period, max_period):

        if profile_type == 'constant':

            variation_gt = start_variation * np.ones((1, number_of_spline_pointer))
            #variation = np.random.normal(start_variation, 0.1, number_of_spline_pointer)
            #variation = np.random.uniform(0.1, 0.5, number_of_spline_pointer)
            #variation[100:200] = 1

            # I want to do some changes here...
            max_ratio = 0.5
            min_ratio = 0.2

            # define the max_period
            my_max_period = 20

            start_phase_list = [0, 0.5*np.pi, np.pi, 1.5*np.pi]
            start_phase = random.sample(start_phase_list, 1)[0]


            period = np.linspace(start_phase, start_phase + my_max_period, number_of_spline_pointer)

            # add the sine and cosine... or we can...
            variation = 0.35 + 0.15 * np.cos(period)

            sel_pos = []
            for _ in range(2):
                rand_sel_pos = random.randint(0, number_of_spline_pointer - 31)
                sel_pos.append(rand_sel_pos)

            for idx in range(len(sel_pos)):
                variation[sel_pos[idx]:sel_pos[idx]+30] = np.random.normal(1.2, 0.1, 30)


        elif profile_type == 'linear':
            up_or_down = random.randint(0, 1)
            if up_or_down == 0:
                max_intensity = random.randint(min(round(start_variation*10), max_axon_intensity*10-1), max_axon_intensity*10-1)/10
                if start_variation != max_intensity:
                    variation = np.arange(start_variation, max_intensity, (max_intensity-start_variation)/(number_of_spline_pointer)).reshape((1, number_of_spline_pointer))
                else:
                    variation = start_variation * np.ones((1, number_of_spline_pointer))

            else:
                min_intensity = random.randint(min_axon_intensity*10+1, max(round(start_variation*10,min_axon_intensity*10+1)))/10
                if start_variation != min_intensity:
                    variation = np.arange(start_variation, min_intensity, (min_intensity-start_variation)/(number_of_spline_pointer)).reshape((1, number_of_spline_pointer))
                else:
                    variation = start_variation * np.ones((1, number_of_spline_pointer))

        elif profile_type == 'cosine':
            number_of_period = random.randint(min_period * 100, max_period * 100) / 100
            if start_variation < 1:
                start_variation = start_variation * 100
            max_intensity = random.randint(min(round(start_variation) - 1, max_axon_intensity-1), max_axon_intensity-1) / 100
            min_intensity = random.randint(min_axon_intensity + 1, max(round(start_variation) + 1, min_axon_intensity + 1)) / 100
            a = 0.5 * (max_intensity + min_intensity)
            b = 0.5 * (max_intensity - min_intensity)
            phi = cmath.acos((round(start_variation) / 100 - a) / b)
            phi = phi.real
            param = np.arange(0, 2 * math.pi * number_of_period, 2 * math.pi * number_of_period / (number_of_spline_pointer))
            up_or_down = random.randint(0, 1)
            if up_or_down == 1:
                #param = np.arange(0, 2 * math.pi * number_of_period, 2 * pi * number_of_period / (number_of_spline_pointer))
                variation = a + b * np.cos(param - phi)
            else:
                variation = a + b * np.cos(param + phi)
        return variation, variation_gt

    #  needs to be added more informations...
    def get_the_image_from_the_distance_matrix(self, distance_matrix, axons_variations):
        axons_variations[axons_variations == float('inf')] = 0
        axons_variations += self.sigma_noise_axon * np.random.randn(self.width, self.height)

        axons_patch = self.varing_intensity_with_distance(distance_matrix, 'axons', 'gauss', self.sigma_spread, axons_variations)
        axons_patch = np.floor(axons_patch * 255 / np.max(axons_patch))

        return axons_patch

    #  needs to be added more informations...
    def varing_intensity_with_distance(self, distance_matrix, structe_type, int_profile_type, sigma_spread, axons_variations,
                                       brightness = 1):
        
        if structe_type == 'axons':
            if int_profile_type == 'butter':
                result = 1 / (1 + (distance_matrix / 2) ** 4)
            elif int_profile_type == 'gauss':
                result = np.exp(-np.square(distance_matrix) / (2 * (sigma_spread ** 2)))
                result = result / np.sqrt(2 * math.pi *(sigma_spread ** 2)) * axons_variations
                result[result <= 0] = 0

        elif structe_type == 'circle':
            if int_profile_type == 'butter':
                result = 1 / (1 + ((distance_matrix / 2) ** 4))
            elif int_profile_type == 'gauss':
                result = np.exp(-distance_matrix**2 / (2*(sigma_spread ** 2))) * brightness

        return result

    def check_crossing(self, axons_dis_without_gap, branch_dis_without_gap, threshold, start_value):
        cross = True
        check_index = np.where((axons_dis_without_gap != float('inf')) & (branch_dis_without_gap != float('inf')))

        if check_index[0].size == 0:
            cross = False
        else:
            len_of_indices = check_index[0].size
            index = 0
            while cross:
                this_row = check_index[0][index]
                this_col = check_index[1][index]
                distance = math.sqrt((start_value[0] - this_col)**2 + (start_value[1] - this_row)**2)
                if distance > threshold:
                    break
                elif index == len_of_indices - 1:
                    cross = False
                else:
                    index += 1
        return cross

    # add the noise....
    def noise(self, patch, axons_patch_without_bouton, sigmawn, lambda_temp, height, width): # has been changed....
        if self.add_border:
            height_real = height + 2 * self.border_size
            width_real = width + 2 * self.border_size
        else:
            height_real = height
            width_real = width

        sigma1 = 0.1
        sigma2 = 6

        size1 = 2
        size2 = 50

        axons_patch_without_bouton = np.floor((axons_patch_without_bouton * 255) / np.max(patch))
        patch = np.floor(patch * 255 / np.max(patch))
        #patch = np.floor(patch * 255 / self.max_value_of_bouton)

        poisson = np.random.poisson(lambda_temp, size=(height_real, width_real))
        gauss = sigmawn * np.random.randn(height_real, width_real)

        oblur1 = utils.matlab_style_gauss2D((size1, size1), sigma1)
        oblur2 = np.round(25 * utils.matlab_style_gauss2D((size2, size2), sigma2), 4)

        patch = scipy.ndimage.filters.convolve(patch, oblur1, mode='nearest') + scipy.ndimage.filters.convolve(poisson, oblur2, mode='nearest') + gauss
        axons_patch_without_bouton = scipy.ndimage.filters.convolve(axons_patch_without_bouton, oblur1, mode='nearest') + scipy.ndimage.filters.convolve(poisson, oblur2, mode='nearest') + gauss

        patch[patch <= 0] = 0
        axons_patch_without_bouton[axons_patch_without_bouton<=0] = 0

        # get the highe
        axons_patch_without_bouton = np.floor((axons_patch_without_bouton * 255) / np.max(patch))
        patch = np.floor(patch * 255 / np.max(patch))

        return patch, axons_patch_without_bouton

    def convert_the_image_into_gray_scale(self, axons_patch):
        gray_scale_image = np.floor(axons_patch * 255 / np.max(axons_patch))
        #gray_scale_image = np.floor(axons_patch * 255 / 255)
        return gray_scale_image

    def convert_to_the_negative_image(self, axons_patch):
        height = axons_patch.shape[0]
        width = axons_patch.shape[1]
        neg_image = 255 * np.ones((height, width)) - axons_patch
        return neg_image

    # load the parameters from the json file
    def _get_params(self):
        self.parameters = self.load_the_json_files(self.paths)
        # here is the parameters...
        self.width = self.parameters['width']
        self.height = self.parameters['height']
        self.negative_image = self.parameters['negative_image']
        self.sigma_noise_min = self.parameters['sigma_noise_min']
        self.sigma_noise_max = self.parameters['sigma_noise_max']
        self.lambda_min = self.parameters['lambdaMin']
        self.lambda_max = self.parameters['lambdaMax']
        self.min_axons = self.parameters['MinAxons']
        self.max_axons = self.parameters['MaxAxons']
        self.min_bran = self.parameters['MinBran']
        self.max_bran = self.parameters['MaxBran']
        self.conformity = self.parameters['conformity']
        self.min_thickness = self.parameters['MinThickness']
        self.max_thickness = self.parameters['MaxThickness']
        self.min_gap_size = self.parameters['MinGapSize']
        self.max_gap_size = self.parameters['MaxGapSize']
        self.step_size = self.parameters['StepSize']
        self.NSpline_points = self.parameters['NSplinePoints']
        self.crossing_ok = self.parameters['crossingOK']
        self.straight_branching = self.parameters['straightBranching']
        self.segmentation_threshold = self.parameters['SegmentationThreshold']
        self.sigma_spread = self.parameters['sigma_spread']
        self.min_axon_intensity = self.parameters['MinAxonIntensity']
        self.max_axon_intensity = self.parameters['MaxAxonIntensity']
        self.min_period = self.parameters['MinPeriod']
        self.max_period = self.parameters['MaxPeriod']
        self.axon_profile = self.parameters['AxonProfile']
        self.branch_profile = self.parameters['BranchProfile']
        self.sigma_noise_axon = self.parameters['sigma_noise_axon']
        self.min_NbBouton = self.parameters['MinNbBouton']
        self.max_NbBouton = self.parameters['MaxNbBouton']
        self.min_bou_radius = self.parameters['MinBouRadius']
        self.max_bou_radius = self.parameters['MaxBouRadius']
        self.min_brightness_bouton = self.parameters['MinBrightnessBouton']
        self.max_brightness_bouton = self.parameters['MaxBrightnessBouton']
        self.sigma_noise_bouton = self.parameters['sigma_noise_bouton']
        self.min_NbCircles = self.parameters['MinNbCircles']
        self.max_NbCircles = self.parameters['MaxNbCircles']
        self.circle_brightness = self.parameters['CircleBrightness']
        self.min_brightness_circles = self.parameters['MinBrightnessBouton']
        self.max_brightness_circles = self.parameters['MaxBrightnessBouton']
        self.min_radius = self.parameters['MinRadius']
        self.max_radius = self.parameters['MaxRadius']
        self.sigma_noise_circle = self.parameters['sigma_noise_circle']
        self.add_noise_requirement = self.parameters['add_noise']
        self.add_cell_requirement = self.parameters['add_cell']
        self.add_bouton_requirement = self.parameters['add_bouton']
        self.add_border = self.parameters['add_border']
        self.border_size = self.parameters['border_size']

        # convert the value in json file into bool...
        if self.crossing_ok == 0:
            self.crossing_ok = False
        else:
            self.crossing_ok = True
        if self.add_noise_requirement == 0:
            self.add_noise_requirement = False
        else:
            self.add_noise_requirement = True
        if self.add_cell_requirement == 0:
            self.add_cell_requirement = False
        else:
            self.add_cell_requirement = True

        if self.add_bouton_requirement == 0:
            self.add_bouton_requirement = False
        else:
            self.add_bouton_requirement = True

        if self.negative_image == 0:
            self.negative_image = False
        else:
            self.negative_image = True
        if self.add_border == 0:
            self.add_border = False
        else:
            self.add_border = True
