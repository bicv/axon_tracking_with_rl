from __future__ import division
import math
import numpy as np
# define some function for the generator...

'''
This file is just the utils...
'''
def mesh_grid(width, height):
    matrix_temp_x = np.zeros((height, width))
    x_value = np.arange(width)
    for index in range(height):
        matrix_temp_x[index, :] = x_value

    matrix_temp_y = np.zeros((height, width))
    for index in range(height):
        matrix_temp_y[index, :] = index

    return matrix_temp_x, matrix_temp_y


def distance_calcu(x, y, point):
    distance = np.sqrt(np.square(x - point[0]) + np.square(y - point[1]))
    return distance


def get_the_min_value_between_two_matrix(a, b):
    temp = np.ones((a.shape[0], a.shape[1]))

    for index_row in range(a.shape[0]):
        for index_col in range(a.shape[1]):
            temp[index_row, index_col] = min(a[index_row, index_col], b[index_row, index_col])

    return temp

def matlab_style_gauss2D(shape=(3,3),sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])

    cite from https://stackoverflow.com/questions/17190649/how-to-obtain-a-gaussian-filter-in-python

    many thanks to the authors...
    """
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h



if __name__ == '__main__':

    print(25*matlab_style_gauss2D((10, 10), sigma=6))

