from env import axons_env
import numpy as np 
import torch
from models import actor_network
import cv2
from arguments import achieve_arguments
"""
This is the demo file of the axon tracking.

"""

# select the action from the beta distribution...
def action_selection(alpha, beta):
    return (alpha - 1) / (alpha + beta - 2)

if __name__ == '__main__':
    args = achieve_arguments()
    path = 'config/parameters_128x128_images.json'
    env = axons_env(path)
    # create the network
    actor_net = actor_network()
    # force load the models to cpu...
    actor_net.load_state_dict(torch.load('saved_models/{}'.format(args.model_name), map_location=lambda storage, loc: storage))
    actor_net.eval()
    for _ in range(1):
        reward_sum = 0
        state, _ = env.reset()
        for step in range(200):
            with torch.no_grad():
                state_tensor = torch.tensor(state, dtype=torch.float32).unsqueeze(0)
                action_alpha, action_beta = actor_net(state_tensor)
            # calculate the entropy...
            action_selected = action_selection(action_alpha, action_beta)
            action_selected_cpu = -4 + action_selected.numpy().squeeze() * 8
            #print action_selected_cpu
            state_, reward, done, _ = env.step(action_selected_cpu)
            reward_sum += reward
            # visiualize the demo..
            left_x = int(round(env.agent.real_pos_x))
            left_y = int(round(env.agent.real_pos_y))
            diff = int((env.size_of_tracker - 1) / 2)
            left_x = left_x - diff
            left_y = left_y - diff
            img = env.entire_image_visual.copy().astype(np.float32)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            cv2.rectangle(img, (left_x, left_y), (left_x + env.size_of_tracker, left_y + env.size_of_tracker), (0, 255, 0), 1)
            cv2.imshow('img', img)
            cv2.waitKey(100)
            if done:
                break
            state = state_ 
        print('reward sum is {}'.format(reward_sum))
