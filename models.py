import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F

# For the Beta_distribution!!!
class actor_network(nn.Module):
    def __init__(self):
        super(actor_network, self).__init__()
        self.conv1 = nn.Conv2d(12, 32, 5, stride=1, padding=2)
        self.conv2 = nn.Conv2d(32, 32, 3, stride=1, padding=2, dilation=1)
        self.affine1 = nn.Linear(32 * 13 * 13, 512)
        # alpha and beta things...
        self.action_alpha = nn.Linear(512, 2)
        self.action_alpha.weight.data.mul_(0.1)
        self.action_alpha.bias.data.mul_(0.0)
        self.action_beta = nn.Linear(512, 2)
        self.action_beta.weight.data.mul_(0.1)
        self.action_beta.bias.data.mul_(0.0)


    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 32 * 13 * 13)
        x = F.relu(self.affine1(x))
        # make action alpha always larger than 0...
        action_alpha = F.softplus(self.action_alpha(x)) + 1
        action_beta = F.softplus(self.action_beta(x)) + 1

        return action_alpha, action_beta

class critic_network(nn.Module):
    def __init__(self):
        super(critic_network, self).__init__()
        self.conv1 = nn.Conv2d(16, 32, 5, stride=1, padding=2)
        self.conv2 = nn.Conv2d(32, 32, 3, stride=1, padding=2, dilation=1)
        # linear part
        self.affine1 = nn.Linear(32 * 13 * 13, 512)
        self.value_head = nn.Linear(512, 1)
        self.value_head.weight.data.mul_(0.1)
        self.value_head.bias.data.mul_(0.0)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 32 * 13 * 13)
        x = F.relu(self.affine1(x))
        state_values = self.value_head(x)

        return state_values
