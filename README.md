# Deep Reinforcement Learning For Axon Tracking
This is the code for MIDL 2019 paper - **Deep Reinforcement Learning For Subpixel Neural Tracking**.

## Requirements
- python 3
- pytorch >= 0.4
- opencv-python
- scipy

## Instruction to run the code 
### Train the network
```bash
python train_network.py

```
### Test the network
```bash
python demo.py --model-name="<model name>"

```
## BibTex
To cite this code for publications - please use:
```
@inproceedings{dai2019deep,
  title={Deep Reinforcement Learning for Subpixel Neural Tracking},
  author={Dai, Tianhong and Dubois, Magda and Arulkumaran, Kai and Campbell, Jonathan and Bass, Cher and Billot, Benjamin and Uslu, Fatmatulzehra and de Paola, Vincenzo and Clopath, Claudia and Bharath, Anil Anthony},
  booktitle={Medical Imaging with Deep Learning},
  year={2019}
}
```