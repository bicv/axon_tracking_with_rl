from env import axons_env
from ppo_agent import ppo_agent
from arguments import achieve_arguments

"""
the script to train the network

"""

if __name__ == '__main__':
    # start to build up the environment...
    path = 'config/parameters_128x128_images.json'
    env = axons_env(path)
    # start to get the agruments...
    args = achieve_arguments()
    # define the ppo...
    ppo_trainer = ppo_agent(env, args)
    ppo_trainer.train()
