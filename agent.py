# this is the class of the agent.. about how agent move....
class tracker():
    def __init__(self, x=0, y=0, visual=False, precision=1):
        # this is the position in the subpixel level...
        self.precision = precision
        self.pos_x = int(x)
        self.pos_y = int(y)
        # this is the coordinate of the real position of x and y
        self.real_pos_x = self.pos_x / (10 ** self.precision)
        self.real_pos_y = self.pos_y / (10 ** self.precision)
        self.visual = visual

    # So in this codition, we want it all in float point...
    def move(self, actions):
        previous_x = self.pos_x
        previous_y = self.pos_y
        # the interpolation pos
        self.pos_x = int(self.pos_x + actions[0])
        self.pos_y = int(self.pos_y + actions[1])
        # the exact position
        self.real_pos_x = self.pos_x / (10 ** self.precision)
        self.real_pos_y = self.pos_y / (10 ** self.precision)
        # during visualization
        if self.visual:
            if self.pos_x == previous_x and self.pos_y == previous_y:
                print('The Tracker Stops!!!!!')